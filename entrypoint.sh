#!/bin/bash

# Wait for the database to be ready
sleep 10

# Apply database migrations
flask db upgrade

# Start the Flask application
flask run --host=0.0.0.0