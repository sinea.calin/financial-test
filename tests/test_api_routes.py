from datetime import datetime
from unittest.mock import patch

import pytest
import requests

from financial.models import FinancialData


class TestApiRoutes:

    @pytest.fixture(autouse=True)
    def create_default_records(self, db):
        records = [
            FinancialData(symbol='IBM', date=datetime.strptime('2023-01-01', '%Y-%m-%d').date(), open_price=100.00,
                          volume=3000, close_price=110.00),
            FinancialData(symbol='AAPL', date=datetime.strptime('2023-01-02', '%Y-%m-%d').date(), open_price=200.00,
                          volume=2000, close_price=210.00),
        ]
        db.session.add_all(records)
        db.session.commit()

    def assert_ibm(self, record):
        assert record['symbol'] == 'IBM'
        assert record['date'] == '2023-01-01'
        assert record['open_price'] == 100.0
        assert record['close_price'] == 110.0
        assert record['volume'] == 3000

    def assert_apple(self, record):
        assert record['symbol'] == 'AAPL'
        assert record['date'] == '2023-01-02'
        assert record['open_price'] == 200.0
        assert record['close_price'] == 210.0
        assert record['volume'] == 2000

    def test_valid_request_with_filter_symbol(self, app, db, testapp):
        response = testapp.get(
            '/api/financial_data?start_date=2023-01-01&end_date=2023-01-31&symbol=IBM&page=1&limit=5')
        data = response.json['data']
        assert response.status_code == 200
        assert len(data) == 1  # Ensure only one record is returned
        record = data[0]
        self.assert_ibm(record)

    def test_data_update_completed_with_server_error(self, app, db, testapp):
        with patch('requests.get') as mock_get:
            mock_get.return_value.raise_for_status.side_effect = requests.exceptions.HTTPError("Server Error")

            response = testapp.get('/api/update_data', expect_errors=True)
            assert response.status_code == 500

    def test_data_update_completed(self, app, db, testapp):
        mock_response = {
            "Time Series (Daily)": {
                "2023-01-01": {
                    "1. open": "100.00",
                    "4. close": "105.00",
                    "6. volume": "1000"
                },
            }
        }

        with patch('requests.get') as mock_get:
            mock_get.return_value.json.return_value = mock_response
            response = testapp.get('/api/update_data')
            assert response.status_code == 200
            data = response.json
            assert data == {'message': 'Data update completed'}
            assert FinancialData.query.count() == 4  # The 2 default rows plus the 2 that were synced for IBM and AAPLE

    def test_data_update_missing_completed(self, app, db, testapp):
        mock_response = {
            "Time Series (Daily)": {
                "2023-01-01": {
                    "1. open": "100.00",
                    "4. close": "105.00",
                },
            }
        }

        with patch('requests.get') as mock_get:
            mock_get.return_value.json.return_value = mock_response
            response = testapp.get('/api/update_data', expect_errors=True)
            assert response.status_code == 500

    def test_root_url_redirection(self, app, testapp):
        response = testapp.get('/')
        assert response.status_code == 302  # Check for the redirection status code
        assert response.location == '/api/financial_data'  # Check if the redirection URL is correct

    def test_valid_request_with_filter_page_and_limit(self, app, db, testapp):
        response = testapp.get(
            '/api/financial_data?start_date=2023-01-01&end_date=2023-01-31&page=1&limit=1')
        data = response.json['data']
        assert response.status_code == 200
        assert len(data) == 1  # Ensure only one record is returned
        record = data[0]
        self.assert_ibm(record)

        response = testapp.get(
            '/api/financial_data?start_date=2023-01-01&end_date=2023-01-31&page=2&limit=1')
        data = response.json['data']
        assert response.status_code == 200
        assert len(data) == 1
        record = data[0]
        self.assert_apple(record)

        response = testapp.get(
            '/api/financial_data?start_date=2023-01-01&end_date=2023-01-31&page=1&limit=2')

        data = response.json['data']
        assert response.status_code == 200
        assert len(data) == 2

    def test_valid_request_with_filter_start_end_date(self, app, db, testapp):
        response = testapp.get(
            '/api/financial_data?end_date=2023-01-1')
        data = response.json['data']
        assert response.status_code == 200
        assert len(data) == 1
        record = data[0]
        self.assert_ibm(record)

        response = testapp.get(
            '/api/financial_data?start_date=2023-01-2')
        data = response.json['data']
        assert response.status_code == 200
        assert len(data) == 1
        record = data[0]
        self.assert_apple(record)

    def test_valid_request_no_params(self, app, db, testapp):
        response = testapp.get(
            '/api/financial_data')
        data = response.json['data']
        assert response.status_code == 200
        assert len(data) == 2

    def test_no_data_found(self, app, db, testapp):
        response = testapp.get(
            '/api/financial_data?start_date=2030-01-01')
        assert response.status_code == 200
        data = response.json['data']
        assert response.status_code == 200
        assert len(data) == 0


class TestStatisticsAPI:

    @pytest.fixture(autouse=True)
    def create_default_records(self, db):
        records = [
            FinancialData(symbol='IBM', date=datetime.strptime('2023-01-01', '%Y-%m-%d').date(), open_price=100.00,
                          volume=3000, close_price=110.00),
            FinancialData(symbol='IBM', date=datetime.strptime('2023-01-01', '%Y-%m-%d').date(), open_price=200.00,
                          volume=1500, close_price=220.00),
        ]
        db.session.add_all(records)
        db.session.commit()

    def test_valid_request_with_all_parameters(self, app, db, testapp):
        response = testapp.get('/api/statistics?start_date=2023-01-01&end_date=2023-01-31&symbol=IBM')
        assert response.status_code == 200
        data = response.json['data']
        assert len(data) == 1
        assert data[0]['average_daily_close_price'] == 165.0  # (110.0 + 220.0) / 2
        assert data[0]['average_daily_open_price'] == 150.0  # (100.0 + 200.0) / 2
        assert data[0]['average_daily_volume'] == 2250  # (3000 + 1500) / 2
        assert data[0]['date'] == '2023-01-01'
        assert data[0]['symbol'] == 'IBM'

    def test_invalid_request_missing_parameters(self, app, db, testapp):
        response = testapp.get('/api/statistics', expect_errors=True)
        assert response.status_code == 400
        result = response.json
        assert result['info'][
                   'error'] == '400 Bad Request: Required parameters (start_date, end_date, symbol) are missing'

    def test_invalid_request_no_data_found(self, app, db, testapp):
        response = testapp.get('/api/statistics?start_date=2030-01-01&end_date=2030-01-31&symbol=IBM',
                               expect_errors=True)
        result = response.json
        assert response.status_code == 400  # This testing framework is not that good it does not know of 204
        assert result['info']['error'] == 'no exception for 204'
