import pytest
from webtest import TestApp

from financial.app import create_app
from financial.extensions import db as _db

@pytest.fixture
def app():
    # Create the Flask application with the "tests.settings" configuration
    _app = create_app("tests.settings")

    # Create a test request context and push it
    ctx = _app.test_request_context()
    ctx.push()

    # Yield the application for testing
    yield _app

    # Pop the request context after the test is completed
    ctx.pop()


@pytest.fixture
def testapp(app):
    # Create a TestApp instance using the Flask application
    return TestApp(app)


@pytest.fixture
def db(app):
    # Set the application of the database to the Flask application
    _db.app = app

    # Create all database tables within the application context
    with app.app_context():
        _db.create_all()

    yield _db

    # Explicitly close the database session and drop all tables
    _db.session.close()
    _db.drop_all()
