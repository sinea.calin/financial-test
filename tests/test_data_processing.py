from datetime import date

from unittest.mock import patch

import pytest
from requests import HTTPError

from get_raw_data import StockDataProcessor
from financial.models import FinancialData


class TestDataProcessor:

    @patch('requests.get')
    def test_get_stock_data(self, mock_get, app):
        mock_response = {
            "Time Series (Daily)": {
                "2023-01-01": {
                    "1. open": "100.00",
                    "4. close": "105.00",
                    "6. volume": "1000"
                },
            }
        }
        mock_get.return_value.json.return_value = mock_response

        processor = StockDataProcessor(app)
        stock_data = processor.get_stock_data('AAPL')

        assert len(stock_data) == 1
        assert stock_data['2023-01-01']['1. open'] == "100.00"
        assert stock_data['2023-01-01']['4. close'] == "105.00"
        assert stock_data['2023-01-01']['6. volume'] == "1000"

    @patch('requests.get')
    def test_get_stock_data(self, mock_get, app):
        mock_response = {
            "Time Series (Daily)": {
                "2023-01-01": {
                    "1. open": "100.00",
                    "4. close": "105.00",
                },
            }
        }
        mock_get.return_value.json.return_value = mock_response

        processor = StockDataProcessor(app)
        with pytest.raises(Exception) as exc_info:
            processor.get_stock_data('AAPL')

        assert exc_info.value.args[0] == 'Invalid stock data format'

    def test_process_stock_data(self, app):
        stock_data = {
            "2023-01-01": {
                "1. open": "100.00",
                "4. close": "105.00",
                "6. volume": "1000"
            },
        }

    def test_get_stock_data_with_failed_request(self, app):
        mock_response = []

        with patch('requests.get') as mock_get:
            mock_get.return_value.json.return_value = mock_response
            mock_get.return_value.raise_for_status.side_effect = HTTPError()
            processor = StockDataProcessor(app)
            with pytest.raises(Exception) as exc_info:
                processor.run()
        assert exc_info.value.args[0] == 'Error occurred while fetching stock data for symbol IBM: '

    def test_script_execution(self, app, db):
        mock_response = {
            "Time Series (Daily)": {
                "2023-01-01": {
                    "1. open": "100.00",
                    "4. close": "105.00",
                    "6. volume": "1000"
                },
            }
        }

        with patch('requests.get') as mock_get:
            mock_get.return_value.json.return_value = mock_response

            processor = StockDataProcessor(app)
            processor.run()

        record = FinancialData.query.filter_by(symbol='AAPL', date=date(2023, 1, 1)).first()
        assert record
        assert record.open_price == 100.00
        assert record.close_price == 105.00
        assert record.volume == 1000

    def test_save_new_record(self, db):
        record = FinancialData(
            symbol='AAPL',
            date=date(2023, 1, 1),
            open_price=100.00,
            close_price=105.00,
            volume=1000
        )
        db.session.add(record)
        db.session.commit()

        saved_record = FinancialData.query.filter_by(symbol='AAPL', date=date(2023, 1, 1)).first()
        assert saved_record
        assert saved_record.open_price == 100.00
        assert saved_record.close_price == 105.00
        assert saved_record.volume == 1000

    def test_save_existing_record(self, db):
        existing_record = FinancialData(
            symbol='AAPL',
            date=date(2023, 1, 1),
            open_price=100.00,
            close_price=105.00,
            volume=1000
        )
        db.session.add(existing_record)
        db.session.commit()

        existing_record.open_price = 110.00
        existing_record.close_price = 115.00
        existing_record.volume = 2000
        db.session.commit()

        saved_record = FinancialData.query.filter_by(symbol='AAPL', date=date(2023, 1, 1)).first()
        assert saved_record
        assert saved_record.open_price == 110.00
        assert saved_record.close_price == 115.00
        assert saved_record.volume == 2000
