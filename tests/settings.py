"""Settings module for testing."""
from financial.settings import *

# Database configuration
SQLALCHEMY_DATABASE_URI = "sqlite:///:memory:"
SQLALCHEMY_TRACK_MODIFICATIONS = False
