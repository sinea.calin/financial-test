# Base image
FROM python:3.9

# Set the working directory in the container
WORKDIR /app

# Copy the requirements file
COPY requirements.txt .

# Install dependencies
RUN pip install -r requirements.txt

# Copy the application code
COPY . .

# Copy the .env file
COPY .env .

# Expose the port on which the Flask app will run
EXPOSE 5000

# Copy the entrypoint script
COPY entrypoint.sh /app/entrypoint.sh

# Grant executable permissions to the entrypoint script
RUN chmod +x /app/entrypoint.sh

# Set the entrypoint script as the command
ENTRYPOINT ["/app/entrypoint.sh"]