from datetime import datetime, timedelta
import requests
from requests import RequestException

from financial.extensions import db
from financial.models import FinancialData

OPEN_PRICE_KEY = "1. open"
CLOSE_PRICE_KEY = "4. close"
VOLUME_KEY = "6. volume"


class StockDataProcessor:
    def __init__(self, application):
        if application:
            self.app = application
        else:
            from financial.app import create_app
            self.app = create_app()
        self.function = "TIME_SERIES_DAILY_ADJUSTED"

    def generate_stock_data_url(self, symbol):
        # Generate the URL for retrieving stock data based on the symbol
        base_url = self.app.config["BASE_URL"]
        function = self.function
        outputsize = "compact"
        api_key = self.app.config["API_KEY"]
        url = f"{base_url}?function={function}&symbol={symbol}&outputsize={outputsize}&apikey={api_key}"
        return url

    def filter_data_by_last_n_days(self, data):
        # Convert date strings to datetime objects
        data = {datetime.strptime(date, '%Y-%m-%d'): values for date, values in data.items()}
        # Calculate date range for the last two weeks
        end_date = max(data.keys())
        start_date = end_date - timedelta(days=self.app.config["DAYS_TO_FETCH"] - 1)
        # Filter the data based on the date range
        filtered_data = {date.strftime('%Y-%m-%d'): values for date, values in data.items() if
                         start_date <= date <= end_date}
        return filtered_data

    def validate_stock_data(self, stock_data):
        # Validate the stock data for any inconsistencies or errors
        for date, values in stock_data.items():
            # Convert the values to the expected types
            try:
                open_price = float(values[OPEN_PRICE_KEY])
                close_price = float(values[CLOSE_PRICE_KEY])
                volume = int(values[VOLUME_KEY])
            except (ValueError, TypeError, KeyError):
                return False
        return True

    def get_stock_data(self, symbol):
        # Fetch stock data from the API for a given symbol
        url = self.generate_stock_data_url(symbol)
        try:
            response = requests.get(url)
            response.raise_for_status()  # Raise an exception if the response status is not successful
            data = response.json()["Time Series (Daily)"]
            filtered_data = self.filter_data_by_last_n_days(data)
            if not self.validate_stock_data(filtered_data):
                raise ValueError("Invalid stock data format")
            return filtered_data
        except RequestException as e:
            raise Exception(f"Error occurred while fetching stock data for symbol {symbol}: {str(e)}")

    def process_stock_data(self, stock_data, symbol):
        # Process the fetched stock data into FinancialData objects
        processed_data = []
        for date, values in stock_data.items():
            financial_data = FinancialData(
                symbol=symbol,
                date=datetime.strptime(date, "%Y-%m-%d").date(),
                open_price=float(values[OPEN_PRICE_KEY]),
                close_price=float(values[CLOSE_PRICE_KEY]),
                volume=int(values[VOLUME_KEY])
            )
            processed_data.append(financial_data)
        return processed_data

    def run(self):
        # Main method to fetch, process, and save stock data
        financial_data = []
        for stock in self.app.config["STOCKS"]:
            stock_data = self.get_stock_data(stock)
            processed_data = self.process_stock_data(stock_data, stock)
            financial_data.extend(processed_data)
        with self.app.app_context():
            for data in financial_data:
                db.session.add(data)
            db.session.commit()


if __name__ == '__main__':
    stock_data_processor = StockDataProcessor()
    stock_data_processor.run()
