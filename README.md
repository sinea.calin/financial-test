# Project Name

## Description

This project is a financial data application that fetches stock data from the AlphaVantage API and 
offers several endpoints for accessing and analyzing the data. To achieve this, Flask is utilized as the web 
framework, SQLAlchemy is chosen as the ORM for database interactions, and the requests library is employed to 
communicate with the stock server.

The project currently maintains a single requirements file, but it can be organized into multiple files or even 
migrated to use Poetry with a pyproject.toml configuration file.

## Improvements

 few improvements can be implemented, but one significant enhancement would be to introduce a 
 custom error handler to handle errors originating from both the command and API contexts. 
 Specifically, I am referring to the errors encountered in the get_raw_data method. Please see the code snippet below for reference:
     ```python

         try:
            if not self.validate_stock_data(filtered_data):
                raise ValueError("Invalid stock data format")
         except RequestException as e:
            raise Exception(f"Error occurred while fetching stock data for symbol {symbol}: {str(e)}")

While this code snippet effectively handles errors in the terminal context, it does not provide a 
meaningful status code when called from the API. To address this, a custom error handler can be implemented to handle both scenarios.



## Tech Stack

The project utilizes the following technologies and frameworks:

- Python
- Flask
- Requests
- PostgreSQL
- Docker

To run the code in your local environment(without Docker), follow these steps:

1. Clone the repository:

   ```shell
   git clone https://gitlab.com/sinea.calin/financial-test.git
   ```
2. Change to the project directory:
    ```shell
    cd financial-test
    ```

3. Install dependencies
    ```shell
   pip install -r requirements.txt
    ```

4. Set up the necessary environment variables:

   Create a .env file in the project root directory.
   Add the following environment variables to the file:

      ```shell
    API_KEY=your_alpha_vantage_api_key
    POSTGRES_USER=your_postgres_user
    POSTGRES_PASSWORD=your_postgres_password
    POSTGRES_DB=your_postgres_db_name
      ```
5. Run the migrations

    ```shell
      flask db upgrade
    ```

6. Run development server

   ```shell
      flask run
   ```

7. Populate data from stock server either by going to /api/update_data or by running

     ```shell
      python get_raw_data.py
   ```

To run the code in your local environment(Docker), follow these steps:

1. Deploy Docker

     ```shell
   docker-compose build   
   docker-compose up
   ```

2. Navigate to localhost:5000/api/update/data to update data

## Managing the API Key

To retrieve financial data from AlphaVantage, you need to obtain an API key.
Here's how to maintain the API key in both local development and production environment:

- Local Development:

Add your API key to the .env file as mentioned in the Local Environment Setup section.\
Make sure to add the .env file to your project's .gitignore to keep it private.

- Production Environment:

Store the API key as an environment variable on your production server.\
Update the production server's environment variable with the API key.\
Use cloud services and store it entities on GCP(example) and use api to populate env at build time

## Tests

1. To run the tests in the tests folder, you can use a test runner like pytest. Here's how you can run the tests:

     ```shell
   pytest tests
   ```