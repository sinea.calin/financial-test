from sqlalchemy import Column, String, Date, Float, Integer
from financial.extensions import db


class FinancialData(db.Model):
    # Define the table name
    __tablename__ = 'financial_data'

    # Define the columns of the table
    id = Column(Integer, primary_key=True)
    symbol = Column(String(10))
    date = Column(Date)
    open_price = Column(Float)
    close_price = Column(Float)
    volume = Column(Integer)

    def __init__(self, symbol, date, open_price, close_price, volume):
        # Initialize the FinancialData object with provided values
        self.symbol = symbol
        self.date = date
        self.open_price = open_price
        self.close_price = close_price
        self.volume = volume

    def save(self):
        # Save the FinancialData object to the database
        existing_record = FinancialData.query.filter_by(symbol=self.symbol, date=self.date).first()
        if existing_record:
            # If a record already exists for the symbol and date, update its values
            existing_record.open_price = self.open_price
            existing_record.close_price = self.close_price
            existing_record.volume = self.volume
        else:
            # If no record exists, add the current object to the session
            db.session.add(self)
        # Commit the changes to the database
        db.session.commit()

    def serialize(self):
        # Serialize the FinancialData object into a dictionary
        return {
            'id': self.id,
            'symbol': self.symbol,
            'date': str(self.date),
            'open_price': self.open_price,
            'close_price': self.close_price,
            'volume': self.volume
        }
