from flask import Flask, url_for, redirect, jsonify

from get_raw_data import StockDataProcessor
from .extensions import (
    migrate,
    db,
)
from .views import financial_data_bp, statistics_bp


def create_app(config_object="financial.settings"):
    app = Flask(__name__.split(".")[0])
    app.config.from_object(config_object)
    register_extensions(app)
    register_blueprints(app)
    register_routes(app)
    return app


def register_blueprints(app):
    app.register_blueprint(financial_data_bp, url_prefix='/api')
    app.register_blueprint(statistics_bp, url_prefix='/api')
    return None


def register_extensions(app):
    db.init_app(app)
    migrate.init_app(app, db)
    return None


def register_routes(app):
    @app.route('/')
    def redirect_to_financial_data():
        return redirect(url_for('financial_data.financial_data'))

    @app.route('/api/update_data')
    def update_data():
        stock_data_processor = StockDataProcessor(app)
        stock_data_processor.run()
        return jsonify({'message': 'Data update completed'})


if __name__ == '__main__':
    app = create_app()
    app.run()
