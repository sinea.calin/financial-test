import os

from environs import Env

env = Env()
# Specify the path to the .env file
env_path = os.path.join(os.path.dirname(__file__), '..', '.env')

# Read the .env file
env.read_env(path=env_path)

# AlphaVantage API configuration
API_KEY = env.str("API_KEY")
BASE_URL = "https://www.alphavantage.co/query"

# Database configuration
DB_HOST = env.str("POSTGRES_HOST", default="localhost")
DB_PORT = env.int("POSTGRES_PORT", default=5432)
DB_NAME = env.str("POSTGRES_DB", default="financial_db")
DB_USER = env.str("POSTGRES_USER", default="postgres")
DB_PASSWORD = env.str("POSTGRES_PASSWORD", default="")
SQLALCHEMY_DATABASE_URI = f"postgresql://{DB_USER}:{DB_PASSWORD}@{DB_HOST}:{DB_PORT}/{DB_NAME}"
SQLALCHEMY_TRACK_MODIFICATIONS = False

# Stock configuration
STOCKS = ["IBM", "AAPL"]
DAYS_TO_FETCH = 14

# API configuration
DEFAULT_PAGE_LIMIT = 5
