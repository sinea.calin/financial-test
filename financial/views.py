from flask import request, jsonify, Blueprint, abort
from flask.views import MethodView
from sqlalchemy import or_, func
from datetime import datetime
from .models import FinancialData


class BaseAPI(MethodView):
    def parse_request_params(self):
        # Parse request parameters
        start_date = request.args.get('start_date')
        end_date = request.args.get('end_date')
        symbol = request.args.get('symbol')
        page = int(request.args.get('page', 1))
        limit = int(request.args.get('limit', 5))
        return start_date, end_date, symbol, page, limit

    def build_query(self, start_date, end_date, symbol):
        # Build the query to retrieve financial data based on the provided parameters
        query = FinancialData.query
        if start_date:
            query = query.filter(FinancialData.date >= datetime.strptime(start_date, '%Y-%m-%d').date())
        if end_date:
            query = query.filter(FinancialData.date <= datetime.strptime(end_date, '%Y-%m-%d').date())
        if symbol:
            query = query.filter(or_(FinancialData.symbol == symbol, FinancialData.symbol.ilike(f'%{symbol}%')))
        return query

    def paginate_query(self, query, page, limit):
        # Paginate the query to retrieve a subset of data
        return query.paginate(page=page, per_page=limit).items

    def build_response(self, data, count, page, limit):
        # Build the response object with data, pagination details, and info
        result = {
            'data': [data_item.serialize() for data_item in data],
            'pagination': {
                'count': count,
                'page': page,
                'limit': limit,
                'pages': count // limit + (1 if count % limit > 0 else 0)
            },
            'info': ''
        }
        return result


class FinancialDataAPI(BaseAPI):
    def get(self):
        try:
            start_date, end_date, symbol, page, limit = self.parse_request_params()
            query = self.build_query(start_date, end_date, symbol)
            count = query.count()
            data = self.paginate_query(query, page, limit)
            result = self.build_response(data, count, page, limit)
            return jsonify(result)

        except Exception as e:
            # Handle exceptions and return an error response
            error_msg = str(e)
            result = {
                'data': [],
                'pagination': {},
                'info': {
                    'error': error_msg
                }
            }
            return jsonify(result), 500


class StatisticsAPI(BaseAPI):
    def get(self):
        try:
            start_date, end_date, symbol, page, limit = self.parse_request_params()
            if not start_date or not end_date or not symbol:
                # Abort the request with a 400 error if required parameters are missing
                abort(400, "Required parameters (start_date, end_date, symbol) are missing")
            statistics = self.calculate_daily_average(start_date, end_date, symbol)
            if not statistics:
                # Abort the request with a 204 status if no data is found
                abort(204, "No data found for the given parameters")
            result = {
                'data': statistics,
                'info': ''
            }
            return jsonify(result)
        except Exception as e:
            # Handle exceptions and return an error response
            error_msg = str(e)
            result = {
                'data': {},
                'info': {
                    'error': error_msg
                }
            }
            return jsonify(result), 400

    def calculate_daily_average(self, start_date, end_date, symbol):
        # Build the base query to retrieve financial data within the specified date range
        query = self.build_query(start_date, end_date, None)

        # Apply an additional filter for the symbol if provided
        if symbol:
            query = query.filter(FinancialData.symbol == symbol)

        # Retrieve the average daily open price for each symbol and date
        average_daily_open_price = query.with_entities(
            FinancialData.symbol,
            FinancialData.date,
            func.avg(FinancialData.open_price)
        ).group_by(
            FinancialData.symbol,
            FinancialData.date
        ).all()

        # Retrieve the average daily close price for each symbol and date
        average_daily_close_price = query.with_entities(
            FinancialData.symbol,
            FinancialData.date,
            func.avg(FinancialData.close_price)
        ).group_by(
            FinancialData.symbol,
            FinancialData.date
        ).all()

        # Retrieve the average daily volume for each symbol and date
        average_daily_volume = query.with_entities(
            FinancialData.symbol,
            FinancialData.date,
            func.avg(FinancialData.volume)
        ).group_by(
            FinancialData.symbol,
            FinancialData.date
        ).all()

        statistics = []
        for open_price in average_daily_open_price:
            symbol, date, avg_open_price = open_price

            # Find the corresponding close price for the symbol and date
            close_price = next(
                (close[2] for close in average_daily_close_price if close[0] == symbol and close[1] == date),
                None
            )

            # Find the corresponding volume for the symbol and date
            volume = next(
                (vol[2] for vol in average_daily_volume if vol[0] == symbol and vol[1] == date),
                None
            )

            # Create a dictionary representing the statistics for the symbol and date
            statistic = {
                'symbol': symbol,
                'date': date.strftime('%Y-%m-%d'),
                'average_daily_open_price': round(avg_open_price, 2),
                'average_daily_close_price': round(close_price, 2) if close_price is not None else None,
                'average_daily_volume': round(volume, 2) if volume is not None else None
            }

            # Append the statistic to the list of statistics
            statistics.append(statistic)

        return statistics


# Create blueprint for the statistics API
statistics_bp = Blueprint('statistics', __name__)

# Create blueprint for the financial data API
financial_data_bp = Blueprint('financial_data', __name__)

# Register the routes for the financial data API
financial_view = FinancialDataAPI.as_view('financial_data')
financial_data_bp.add_url_rule('/financial_data', view_func=financial_view, methods=['GET'])

# Register the routes for the statistics API
statistics_view = StatisticsAPI.as_view('statistics')
statistics_bp.add_url_rule('/statistics', view_func=statistics_view, methods=['GET'])
